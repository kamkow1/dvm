#ifndef NBPP_H_
#define NBPP_H_

#include <iomanip>
#include <ctime>
#include <chrono>
#include <functional>
#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <cctype>
#include <tuple>
#include <string>
#include <filesystem>
#include <iostream>
#include <vector>
#include <variant>
#include <utility>
#include <unordered_map>
#include <sstream>
#include <cstdio>
#include <fstream>

namespace nb {

// Re-exports
using fs_dir_iter = std::filesystem::recursive_directory_iterator;
using fs_dir_ent = std::filesystem::directory_entry;

// ---------------------------------------
// Logging
// ---------------------------------------

// log command in format `[CMD] ...`
void log_cmd(std::string s) { std::cerr << "\x1b[32m" << "[CMD] " << "\x1b[39m" << s << std::endl; }

// log error in format `[ERROR] ...`
void log_error(std::string s) { std::cerr << "\x1b[31m" << "[ERROR] " << "\x1b[39m" << s << std::endl; }

// log information in format `[INFO] ...`
void log_info(std::string s) { std::cerr << "\x1b[36m" << "[INFO] " << "\x1b[39m" << s << std::endl; }

// log information in format `[WARNING] ...`
void log_warning(std::string s) { std::cerr << "\x1b[33m" << "[WARNING] " << "\x1b[39m" << s << std::endl; }

// ---------------------------------------
// String Utilities 
// ---------------------------------------

std::vector<std::string> split(const std::string &s, char dlm)
{
	std::vector<std::string> result;
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, dlm)) result.push_back(item);
	return result;
}

inline bool is_int(const std::string &s)
{
	if(s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+'))) return false;
	char * p;
	std::strtol(s.c_str(), &p, 10);
	return (*p == 0);
}

inline bool is_float(const std::string &s)
{
	// no dot found - not a float
	if (s.find('.') == std::string::npos) return false;
	if(s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+'))) return false;
	char * p;
	std::strtof(s.c_str(), &p);
	return (*p == 0);
}

inline bool is_boolean(const std::string &s)
{
	for (const char &letter: s) if (std::isupper(letter)) return false;
	if (s == "true" || s == "false") return true;
	else return false;
}

template<typename T>
T x_from_string(const std::string &s)
{
	std::istringstream ss(s);
	T ret;
	ss >> ret;
	return ret;
}

// ---------------------------------------
// Configuration
// ---------------------------------------

class Configuration
{
public:

	// absolute paths to programs
	std::string recom_compiler;
	std::string compiler;
	std::string linker;
	std::string archiver;

	Configuration(void)
	: recom_compiler("g++")
	, compiler("gcc")
	, linker("gcc")
	, archiver("ar")
	{
	}

	Configuration(
		std::string _recom_compiler,
		std::string _compiler,
		std::string _linker,
		std::string _archiver
	)
	: recom_compiler(_recom_compiler)
	, compiler(_compiler)
	, linker(_linker)
	, archiver(_archiver)
	{
	}
};

// defined nb::my_configuration, which then can be accessed by the user
#ifdef CONFIGURATION
#include CONFIGURATION
#else
Configuration my_configuration; // default configuration
#endif

// ---------------------------------------
// Flags
// ---------------------------------------

class Flags: public std::vector<std::string>
{
private:
	void push_flag(std::string f) { this->push_back(f); }
	void push_flag(const char *f) { this->push_back(std::string(f)); }
public:
	template<typename ...T>
	void push(T ...flags) { (this->push_flag(flags), ...); }
	void clear_flags(void) { this->clear(); }
	template<typename T>
	void push_vector(std::vector<T> v) {
		for (const T &t: v) this->push_flag(t);
	}
	std::string string(void)
	{
		std::string flags = " ";
		for (const std::string &s: *this) {
			flags += s;
			flags += " ";
		}
		return flags;
	}

	Flags(void) = default;

	template<typename T>
	Flags(std::vector<T> v)
	{
		this->push_vector(v);	
	}
};

// ---------------------------------------
// Os wrappers
// ---------------------------------------

// std::system wrapper
bool run_cmd(std::string cmd)
{
	log_cmd(cmd);
	if (std::system(cmd.c_str()) != 0) {
		log_error("Command failed: " + cmd);
		return false;
	}
	return true;
}	

// `cp` wrapper
bool cp(const char *a, const char *b)
{
	std::string cmd = "cp";
	Flags flags;
	flags.push(a, b);
	cmd += flags.string();
	return run_cmd(cmd);
}

// `mv` wrapper
bool mv(const char *a, const char *b)
{
	std::string cmd = "mv";
	Flags flags;
	flags.push(a, b);
	cmd += flags.string();
	return run_cmd(cmd);
}

template<typename T>
std::string slurp_file(T path)
{
	std::ifstream ifs(path);
	return std::string(
		std::istreambuf_iterator<char>(ifs),
		std::istreambuf_iterator<char>()
	);
}




// ---------------------------------------
// Resolving Dependencies
// ---------------------------------------

// NOT CORSSPLATFORM! find an executable in a possible location (restricted to Linux and alike).
std::tuple<bool, std::string> find_program(std::string exe, std::vector<std::string> custom_search_paths)
{
	std::vector<std::string> search_paths = {
		"/bin/",
		"/usr/bin/",
		"/usr/local/bin/",
	};
	for (const std::string &csp: custom_search_paths) {
		if (csp.back() != '/') {
			log_error("Invalid path. Custom search path MUST end with '/'!");
			return std::make_tuple(false, "");
		}
		search_paths.push_back(csp);
	}
	for (const std::string &sp: search_paths) {
		std::string full_path = sp + exe;
		if (std::filesystem::exists(split(full_path, ' ')[0])) {
			return std::make_tuple(true, full_path);
		}
	}
	return std::make_tuple(false, "");
}

#ifdef NBPP_CACHE
#include NBPP_CACHE
#endif

// Collect the source files for compilation, exclude unwanted files
std::vector<std::string> src(const char *ext, std::vector<std::string> exclude)
{
	std::vector<std::string> src;

#ifdef NBPP_CACHE
	log_info(std::string("Cache file: ") + NBPP_CACHE);
	std::ofstream _cache(std::filesystem::path(NBPP_CACHE).filename(), std::ios::trunc);
	if (!_cache) log_error("Cannot open cache file for clearing");
	_cache << "";
	_cache.flush();
	_cache.close();

	std::ofstream cache(std::filesystem::path(NBPP_CACHE).filename(), std::ios::app);
	cache << "#ifndef NBPP_CACHE_H_" << std::endl;
	cache << "#define NBPP_CACHE_H_" << std::endl;
	cache << std::endl;
	cache << "#include <vector>" << std::endl;
	cache << "#include <string>" << std::endl;
	cache << "#include <tuple>" << std::endl;
	cache << "#include <ctime>" << std::endl;
	cache << std::endl;
	cache << "namespace _nb_cache {" << std::endl;
	cache << "std::vector<std::tuple<std::string, std::time_t>> cache = {" << std::endl;
#endif
	for (const fs_dir_ent &ent: fs_dir_iter(".")) {
		if (ent.is_regular_file()) {
			const std::filesystem::path &p = ent.path();
			const std::string pstr = p.string();
			const std::filesystem::path pstrc = std::filesystem::canonical(pstr);

			for (const std::string &exclude_file: exclude) {
				const std::filesystem::path efc = std::filesystem::canonical(exclude_file);
				if (efc == pstrc) goto cnt;
			}

#if defined(NBPP_CACHE) && defined(NBPP_CACHE_H_)
			for (const std::tuple<std::string, std::time_t> &cached_file: _nb_cache::cache) {
				std::string path = std::get<0>(cached_file);
				std::time_t cached_time = std::get<1>(cached_file);
				if (pstrc.string() == path) {
					log_info("Cached tuple: path = " + path + ", timestamp = " + std::to_string(cached_time));
					const std::filesystem::file_time_type modified = std::filesystem::last_write_time(pstrc);
					const std::time_t time = std::chrono::system_clock::to_time_t(std::chrono::file_clock::to_sys(modified));

					if (time == cached_time) { // file hasn't changed
						goto cnt;
					}
				}
			}
#endif

			if (p.extension() == ext) {
				src.push_back(pstr);
#ifdef NBPP_CACHE
				const std::filesystem::file_time_type modified = std::filesystem::last_write_time(pstrc);
				const std::time_t time = std::chrono::system_clock::to_time_t(std::chrono::file_clock::to_sys(modified));
				cache << "    " << "{\"" << pstrc.string() << "\", " << time << "}," << std::endl;
#endif
			}
		}
cnt: 
		;
	}
#ifdef NBPP_CACHE
	cache << "};" << std::endl;
	cache << "}" << std::endl; // _nb_cache
	cache << "#endif // NBPP_CACHE_H_" << std::endl;

	cache.flush();
	cache.close();
#endif
	return src;
}

// ---------------------------------------
// Compiling and Linking
// ---------------------------------------

#define NB_FLAGS_FIRST true

// Macros:
// the `nb::compile()` and `nb::link()` functions can take macros as implicit parameters.
// A macro is pushed into the flag vector by the user
// Syntax:
// #<macro-name>#<args>
//
// Macro specific info:
// - object macro
// This macro is used to convert source file paths into object file paths, while iterating
// through them. It simply changes the file extension in the path, so for example `my/file.c` becomes `my/file.o`.
// - source macro
// Emits the provided source file path that is used while iterating through source files.

enum MacroIndex
{
	MI_OBJECT,
	MI_SOURCE,
};

static const char *macros[] =
{
	"object",
	"source",
};

// links the final executable from object files.
void link(
	std::vector<std::string> input_objs,
	Configuration &configuration,
	Flags &flags,
	bool flags_first = false
)
{
	Flags objs(input_objs);
	if (objs.size() == 0) return;
	std::string cmd = flags_first
			? (configuration.linker + flags.string() + objs.string())
			: (configuration.linker + objs.string() + flags.string());
	run_cmd(cmd);
}

void archive(
	std::vector<std::string> input_objs,
	Configuration &configuration,
	Flags &flags,
	bool flags_first = false
)
{
	Flags objs(input_objs);
	if (objs.size() == 0) return;
	std::string cmd = flags_first
			? (configuration.archiver + flags.string() + objs.string())
			: (configuration.archiver + objs.string() + flags.string());
	run_cmd(cmd);
}

// Compiles the source files into object files.
std::vector<std::string> compile(
	std::vector<std::string> &srcs,
	Configuration &configuration,
	Flags &flags
)
{
	std::vector<std::string> obj;
	for (const std::string s: srcs) {
		log_info("source - " + s);
		Flags exp_flags;
		for (std::string f: flags) {
			if (f.find("#") < f.length()) {
				std::vector<std::string> data = split(f, '#');
				std::string prefix = data[0];
				std::string macro = data[1];
				log_info("Expanding macro: " + macro);
				if (macro == macros[MI_OBJECT]) {
					std::string extension = data[2];
					std::filesystem::path p = s;
					p.replace_extension(extension);
					std::string pstr = p.string();
					f = prefix + pstr;
					log_info("object - " + pstr);
					obj.push_back(pstr);
				} else if (macro == macros[MI_SOURCE]) {
					f = std::filesystem::path(s).filename();	
				}
			}
			exp_flags.push(f);
		}
		exp_flags.push(s);
		std::string cmd = configuration.compiler + exp_flags.string();
		run_cmd(cmd);
	}
	return obj;
}

// Recompile Nb++. Move the old Nb++ into `nb++-old`, compile as `nb++`
// if failed, revert back to the previous build
bool recompile_self(int argc, char **argv, Configuration &configuration)
{
	mv(argv[0], "nb++-old");
	std::string cmd = configuration.recom_compiler;
	Flags flags;
	flags.push("nb++.cpp", "-o", argv[0], "-std=c++20");
	cmd += flags.string();
	bool ec = run_cmd(cmd);
	if (!ec) mv("nb++-old", argv[0]);
	return ec;
}

// returns a hash of the build system source code.
// The actual hash is just the two nb++.cpp and nb++.hpp combined
std::string get_hash_nbppcpp_nbpphpp(void)
{
	std::string hash;
	hash += slurp_file("nb++.cpp");
	hash += slurp_file(__FILE__);
	std::function check_space = [](unsigned char x) { return std::isspace(x); };
	hash.erase(std::remove_if(hash.begin(), hash.end(), check_space), hash.end());
	return hash;
}

// checks for changes. If the `nb++-src-hash.txt` does not exist, it creates one.
// This file contains the hash of the previously built source code, so we can compare it later
std::tuple<bool /* ok */, bool /* result */> check_if_changed(void)
{
	std::string hash_file_name = "./nb++-src-hash.txt";
	log_info("Source hash filename: " + hash_file_name);
	
	std::string hash = get_hash_nbppcpp_nbpphpp();

	if (!std::filesystem::exists(hash_file_name)) {
		log_info(hash_file_name + " doesn't exists. creating the file...");
		std::ofstream(hash_file_name) << "";
		std::fstream hash_file(hash_file_name, std::ios::out | std::ios::in | std::ios::trunc);
		hash_file << hash;
		return std::make_tuple(true, true);
	}

	std::string existing_hash = slurp_file(hash_file_name);
	if (hash == existing_hash) return std::make_tuple(true, false);
	std::fstream hash_file(hash_file_name, std::ios::out | std::ios::in | std::ios::trunc);
	hash_file << hash;
	return std::make_tuple(true, true);
}

// reruns the build program. This fundtion should be called after recompiling
void rerun_self(int argc, char **argv)
{
	Flags flags;
	for (size_t i = 0; i < (size_t)argc; ++i) {
		flags.push(argv[i]);
	}
	std::exit(run_cmd(flags.string()));
}

// ---------------------------------------
// Simple commandline argument parser
// ---------------------------------------
// 
// Syntax:
// program -option value -option2 value2

typedef std::variant<
			std::string,
			int32_t,
			float,
			bool> OptValue;

typedef std::unordered_map<std::string, OptValue> OptMap;

std::tuple<bool, OptMap> parse_options(int argc, char **argv)
{
	OptMap map;
	// an option MUST have a corresponding value
	if ((argc-1)%2 != 0) return std::make_tuple(false, map);

	for (size_t i = 1; i < (size_t)argc; ++i) {
		char *token = argv[i];
		char *optname;
		if (token[0] == '-') { // option name
			optname = ++token; // forget `-`
		} else { // option value
			OptValue value;
			std::string strtoken(token);
			if (strtoken[0] == '\'') {
				strtoken.erase(0, 1);
				std::string buf;
				size_t i = 0;
				char c;
				while ((c = strtoken[i++]) != '\'') buf += c;
				value = buf;
			} else {
				if (strtoken == "true") {
					value = true;
				} else if (strtoken == "false") {
					value = false;
				} else {
					if (is_float(strtoken)) { // float
						value = x_from_string<float>(strtoken);
					} else if (is_int(strtoken)) { // int
						value = x_from_string<int32_t>(strtoken);
					} else if (is_boolean(strtoken)) { // boolean
						value = x_from_string<bool>(strtoken);
					} else { // string
						value = strtoken;
					}
				}
			}
			// an option is always followed by a value
			map.insert({optname, value});
		}
	}

	return std::make_tuple(true, map);
}

} // nb

#endif // NBPP_H_
