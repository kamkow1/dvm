# Dvm - dumb virtual machine

Dvm is a simple stack-based virtual machine written in C.

## roadmap

Here's a simple list of what I'd like to achieve with this project:
- [ ] Working instruction set:
    * [ ] stack operations
        - [x] push
        - [x] pop
        - [ ] dup
        - [ ] swap
    * [x] basic math
        - [x] add
        - [x] sub
        - [x] mul
        - [x] div
    * [x] structs
        - [x] creating structs
        - [x] accessing the fields
    * [ ] comparing values
    * [x] goto/jump
        - [x] goto
        - [x] jmprel
    * [ ] addressing memory
    * [ ] functions
        - [ ] call
        - [ ] ret
- [x] Simple assembler language
- [x] Code generation library (see libdvmcodegen)
- [ ] Native interface
- [ ] Port of TCC or any simple C compiler (LLVM and GCC are too much tbh)

## components

### libdvmcodegen

Libdvmcodgen is a code generation library. It's used to serialize Dvm's modules into bytes and vice versa.

### libcommon

Common C utilities used by all components of the Dvm project. For example, see `libcommon/da.h` - a simple dynamic array implementation for.

### vm

Implementation of the Dvm virtual machine

### as

Dvm's assembler. It aims to stay simple/minimalistic

## assembly

To learn more about Dvm's instruction set, checkout `as/tests/math.dvmas` or `as/tests/struct.dvmas`.

## build system - nb++.hpp

The entire build system is contained within the nb++.hpp header file. Here's how to use it to compile a component:

```console
cd <project-name>
c++ -std=c++20 -o nb++ ./nb++.cpp
./nb++ -sc <subcommand> <other options>
```

By convention to build a component you'd use `-sc build`, but nothing prevents you from changing the subcommand's name.

NOTE: you ARE required to use a compiler that supports C++20 or newer. To learn more about nb++, see https://gitlab.com/kamkow1/nbpp
