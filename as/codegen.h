#ifndef CODEGEN_H_
#define CODEGEN_H_

#include "token.h"
#include "libdvmcodegen/module.h"

// generates a module structure from Token_Stream
Dvmcg_Module codegen(Token_Stream *ts);

#endif // CODEGEN_H_
