#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "instrs.h"
#include "token.h"
#include "codegen.h"
#include "component.h"
#include "libcommon/str.h"
#include "libcommon/da.h"
#include "libcommon/log.h"

// caller has to free the memory
char *replace_filename_ext(const char *s, const char *new_ext)
{
	char *ext;
	char *tmp = strdup(s);
	ext = strrchr(tmp, '.');
	if (ext) *ext = '\0';
	size_t new_size = strlen(tmp)+strlen(new_ext)+1;
	char *new_name = MALLOC(new_size);
	sprintf(new_name, "%s%s", tmp, new_ext);
	free(tmp);
	return new_name;
}

int main(int argc, char **argv)
{
	const char *input_file_path = argv[1];

	FILE *input_file = fopen(input_file_path, "r");
	if (input_file == NULL) LOG_FATAL(COMPONENT, "Cannot open input file %s: %s", input_file_path, strerror(errno));

	Token_Stream ts;
	da_init(Token, &ts, 1);

	char linebuf[0xff];
	memset(linebuf, 0, sizeof(linebuf));
	const char *delims = " \t\r\n\v\f";
	while (fgets(linebuf, sizeof(linebuf), input_file)) {
		if (linebuf[0] == '/' && linebuf[1] == '/') continue;

		char *rest = NULL;
		char *token = NULL;
		for ((token = strtok_r(linebuf, delims, &rest));
			token != NULL;
			token = strtok_r(NULL, delims, &rest)) {
			da_append(Token, &ts, (Token){.string = str_from_cstr(token)});
		}
	}

	Dvmcg_Module module = codegen(&ts);
	Dvmcg_Bytes bytes = module_serialize(&module);
	LOG_INFO(COMPONENT, "generated a module of %zu bytes", bytes.count);

	const char *output_file_path = replace_filename_ext(input_file_path, ".dvmobj");
	FILE *output_file = fopen(output_file_path, "wb+");
	if (input_file == NULL) LOG_FATAL(COMPONENT, "Cannot open output file %s: %s", output_file_path, strerror(errno));

	for (size_t i = 0; i < bytes.count; i++) {
		size_t n = fwrite(&bytes.items[i], 1, sizeof(unsigned char), output_file);
		if (n == 0) LOG_FATAL(COMPONENT, "Failed to write to output file %s (written %zu bytes)", output_file_path, n);
	}

	ts_deinit(&ts);
	module_deinit(&module);
	da_deinit(&bytes);
	fclose(output_file);
	fclose(input_file);
	return 0;
}
