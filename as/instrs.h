#ifndef INSTRS_H_
#define INSTRS_H_

#include <stdint.h>

#include "libdvmcodegen/instrs.h"

#define NOT_INSTR 2000

static const char *INSTRS[] = {
	[IS_PUSH] = "push",
	[IS_POP] = "pop",

	[IS_KILL] = "kill",
	[IS_HANG] = "hang",

	[IS_ADD] = "add",
	[IS_SUB] = "sub",
	[IS_MUL] = "mul",
	[IS_DIV] = "div",

	[IS_STRUCT_CREATE] = "struct_create",
	[IS_STRUCT_PUSH_FIELD] = "struct_push_field",

	[IS_GOTO] = "goto",
	[IS_JMPREL] = "jmprel",
	[IS_RET] = "ret",
};

#endif // INSTRS_H_
