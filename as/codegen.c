#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "codegen.h"
#include "token.h"
#include "component.h"
#include "libcommon/str.h"
#include "libcommon/da.h"
#include "libcommon/log.h"
#include "libdvmcodegen/instrs.h"
#include "libdvmcodegen/module.h"

// represents a single label
// instr_idx holds an index within the module array
// which points at the instruction right after the label
typedef struct {
	Str string;
	size_t instr_idx;
} Label;

// a collection of labels
typedef struct {
	Label *items;
	size_t count;
	size_t cap;
} Labels;

#define labels_deinit(ls) \
{ \
	for (size_t i = 0; i < (ls)->count; i++) { \
		da_deinit(&(ls)->items[i].string); \
	} \
	da_deinit((ls)); \
} \

Dvmcg_Module codegen(Token_Stream *ts)
{
#define noop_instr(itp) \
{ \
	Dvmcg_Meta_Instr instr = {0}; \
	da_init(Dvmcg_Primitive, &instr, 0); \
	instr.type = (itp); \
	da_append(Dvmcg_Meta_Instr, &module, instr); \
} \

#define handle_tt_label_ref() \
{ \
	bool found = false; \
	for (size_t i = 0; i < labels.count; i++) { \
		Label l = labels.items[i]; \
		if (strcmp(l.string.items, token->string.items+1) == 0) { \
			LOG_DEBUG(COMPONENT, "referencing label %s+0x%04x", token->string.items+1, (unsigned int)l.instr_idx); \
			const Dvmcg_Primitive operand = (Dvmcg_Primitive)l.instr_idx; \
			da_append(Dvmcg_Primitive, &instr, operand); \
			found = true; \
		} \
	} \
	if (!found) LOG_ERROR(COMPONENT, "label doesn't exist: %s", token->string.items+1); \
} \

	Dvmcg_Module module;
	da_init(Dvmcg_Meta_Instr, &module, 1);

	Labels labels;
	da_init(Label, &labels, 1);

	Token *token = NULL;
	while ((token = ts_next_token(ts)) != NULL) {
		token_classify(token);

		switch (token->type) {
		case TT_LABEL_BEGIN:
			{
				Label label = (Label){.string = str_from_cstr(token->string.items+1), .instr_idx = module.count};
				da_append(Label, &labels, label);

				Label l = labels.items[labels.count-1];
				LOG_DEBUG(COMPONENT, "new label: %s+0x%04x", l.string.items, (unsigned int)l.instr_idx);
			} break; // TT_LABEL_BEGIN
		case TT_INSTR:
			{
				switch (token->instr_type) {
				case IS_PUSH:
					{
						Dvmcg_Meta_Instr instr = {0};
						da_init(Dvmcg_Primitive, &instr, 1);
						instr.type = IS_PUSH;
						token = ts_next_token(ts);
						if (token == NULL) LOG_FATAL(COMPONENT, "unexpected end of token stream");
						token_classify(token);
#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wswitch"
#endif	
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wswitch"
#endif
						switch (token->type) {
						case TT_INT:
							{
								char *end;
								const Dvmcg_Primitive operand = (Dvmcg_Primitive)strtol(token->string.items, &end, 10);
								da_append(Dvmcg_Primitive, &instr, operand);
							} break; // TT_INT
						case TT_FLOAT:
							{
								char *end;
								const Dvmcg_Primitive operand = (Dvmcg_Primitive)strtod(token->string.items, &end);
								da_append(Dvmcg_Primitive, &instr, operand);
							} break; // TT_FLOAT
						case TT_LABEL_REF: handle_tt_label_ref(); break;
						default:
							LOG_ERROR(COMPONENT, "`push` instructions requires a numeric/label ref operand. got: %s", token->string.items);
							break;
						}
#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif	
#ifdef __clang__
#pragma clang diagnostic pop
#endif
						da_append(Dvmcg_Meta_Instr, &module, instr);
					} break; // IS_PUSH
				case IS_POP: noop_instr(IS_POP); break; // IS_POP
				case IS_ADD: noop_instr(IS_ADD); break; // IS_ADD
				case IS_SUB: noop_instr(IS_SUB); break; // IS_SUB
				case IS_DIV: noop_instr(IS_DIV); break; // IS_DIV
				case IS_MUL: noop_instr(IS_MUL); break; // IS_MUL
				case IS_KILL: noop_instr(IS_KILL); break; // IS_KILL
				case IS_HANG: noop_instr(IS_HANG); break; // IS_HANG
				case IS_STRUCT_CREATE: noop_instr(IS_STRUCT_CREATE); break; // IS_STRUCT_CREATE 
				case IS_STRUCT_PUSH_FIELD: noop_instr(IS_STRUCT_PUSH_FIELD); break; // IS_STRUCT_PUSH_FIELD

				// flow
				case IS_GOTO: noop_instr(IS_GOTO); break; // IS_GOTO
				case IS_JMPREL: noop_instr(IS_JMPREL); break; // IS_JMPREL
				case IS_RET: noop_instr(IS_RET); break; // IS_RET

				case _size:
					LOG_FATAL(COMPONENT, "unreachable");
					break;
				}
			} break; // TT_INSTR
		default:
			LOG_FATAL(COMPONENT, "TODO: unimplemented token type: %d\n", token->type);
			break;
		}
	}
	labels_deinit(&labels);
	return module;
}
