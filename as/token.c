#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

#include "token.h"
#include "instrs.h"
#include "component.h"
#include "libcommon/log.h"

bool check_int(const char *s)
{
	char c;
	while ((c = *s++)) {
		if (c == '-' || c == '+') continue;
		if (!isdigit(c)) return false;
	}
	return true;
}

bool check_float(const char *s)
{
	char c;
	while ((c = *s++)) {
		if (c == '-' || c == '+' || c == '.') continue;
		if (!isdigit(c)) return false;
	}
	return true;
}

void token_classify(Token *t)
{
	if (t->string.items[0] == '#') { t->type = TT_LABEL_BEGIN; return; }
	if (t->string.items[0] == '@') { t->type = TT_LABEL_REF; return; }

	for (size_t i = 0; i < sizeof(INSTRS)/sizeof(INSTRS[0]); i++) {
		if (strcmp(t->string.items, INSTRS[i]) == 0) { t->type = TT_INSTR; t->instr_type = i; return; }
	}

	if (check_int(t->string.items)) { t->type = TT_INT; t->instr_type = NOT_INSTR; return; }
	if (check_float(t->string.items)) { t->type = TT_FLOAT; t->instr_type = NOT_INSTR; return; }

	LOG_FATAL(COMPONENT, "Failed to classify token: %s", t->string.items);
}

Token *ts_next_token(Token_Stream *ts)
{
	if (ts->current_index == ts->count) return NULL;
	return &ts->items[ts->current_index++];
}

