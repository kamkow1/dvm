#ifndef TOKEN_H_
#define TOKEN_H_

#include <stddef.h>
#include <stdbool.h>

#include "libcommon/str.h"
#include "libcommon/da.h"
#include "libdvmcodegen/instrs.h"

typedef enum {
	TT_INSTR,
	TT_INT,
	TT_FLOAT,
	TT_LABEL_BEGIN,
	TT_LABEL_REF,
} Token_Type;

typedef struct {
	Str string;
	Token_Type type;
	Dvmcg_Instr_Set instr_type;
} Token;

typedef struct {
	Token *items;
	size_t count;
	size_t cap;
	size_t current_index;
} Token_Stream;

#define ts_deinit(ts) \
{ \
	for (size_t i = 0; i < (ts)->count; i++) { \
		da_deinit(&(ts)->items[i].string); \
	} \
	da_deinit((ts)); \
} \

// takes a single token and gives a meaningful type (Token_Type)
void token_classify(Token *t);
// returns the next token in Token_Stream. If there are no tokens left, returns NULL
Token *ts_next_token(Token_Stream *ts);
// check if a string literal is an integer
bool check_int(const char *s);
// check if a string literal is a float
bool check_float(const char *s);

#endif // TOKEN_H_
