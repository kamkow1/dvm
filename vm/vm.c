#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <stddef.h>

#include "vm.h"
#include "component.h"
#include "libdvmcodegen/instrs.h"
#include "libdvmcodegen/module.h"
#include "libcommon/da.h"
#include "libcommon/log.h"

Vm_State VM_STATE = {0};
Vm_Stack VM_STACK = {0};
Dvmcg_Module *MODULE = NULL;

void vm_deinit_state(void) { vm_init_state(); } // reset vm's state
void vm_init_state(void)
{
	VM_STATE.running = true;
	VM_STATE.pc = 0;
}

void vm_init(Dvmcg_Module *m)
{
	MODULE = m;
	da_init(Vm_Stack_Object, &VM_STACK, 1);
	vm_init_state();
}

void vm_deinit(void)
{
	MODULE = NULL;
	for (size_t i = 0; i < VM_STACK.count; i++) {
		Vm_Stack_Object obj = VM_STACK.items[i];
		if (obj.type == VSOT_STRUCT) {
			da_deinit(&obj.data.strct);
		}
	}
	da_deinit(&VM_STACK);
	vm_deinit_state();
}

Dvmcg_Meta_Instr *vm_instr_fetch(void) {
	if (VM_STATE.pc == MODULE->count) return NULL;
	return &MODULE->items[VM_STATE.pc++];
}

void vm_instr_eval(const Dvmcg_Meta_Instr *instr)
{
	switch (instr->type) {
		// stack
	case IS_PUSH:
		{
			Vm_Stack_Object so = (Vm_Stack_Object){.type = VSOT_PRIMITIVE, .data = (Vm_Data){.prm = instr->items[0]}};
			da_append(Vm_Stack_Object, &VM_STACK, so);
		} break;
	case IS_POP:
		(void)da_pop(&VM_STACK);
		break;
		
		// vm state
	case IS_HANG:
		while(true);
		break;
	case IS_KILL:
		VM_STATE.running = false;
		break;

		// math
	case IS_ADD:
		{
			Vm_Stack_Object _a = da_pop(&VM_STACK);
			Vm_Stack_Object _b = da_pop(&VM_STACK);
			if (_a.type != VSOT_PRIMITIVE || _b.type != VSOT_PRIMITIVE) {
				LOG_ERROR(COMPONENT, "`add` instruction expects a primitvie value");
			}
			Vm_Data a = _a.data;
			Vm_Data b = _b.data;
			Vm_Stack_Object so = (Vm_Stack_Object){.type = VSOT_PRIMITIVE, .data = (Vm_Data){.prm = a.prm+b.prm}};
			da_append(Vm_Stack_Object, &VM_STACK, so);
		} break; // IS_ADD
	case IS_SUB:
		{
			Vm_Stack_Object _a = da_pop(&VM_STACK);
			Vm_Stack_Object _b = da_pop(&VM_STACK);
			if (_a.type != VSOT_PRIMITIVE || _b.type != VSOT_PRIMITIVE) {
				LOG_ERROR(COMPONENT, "`sub` instruction expects a primitvie value");
			}
			Vm_Data a = _a.data;
			Vm_Data b = _b.data;
			Vm_Stack_Object so = (Vm_Stack_Object){.type = VSOT_PRIMITIVE, .data = (Vm_Data){.prm = a.prm-b.prm}};
			da_append(Vm_Stack_Object, &VM_STACK, so);
		} break; // IS_SUB
	case IS_MUL:
		{
			Vm_Stack_Object _a = da_pop(&VM_STACK);
			Vm_Stack_Object _b = da_pop(&VM_STACK);
			if (_a.type != VSOT_PRIMITIVE || _b.type != VSOT_PRIMITIVE) {
				LOG_ERROR(COMPONENT, "`mul` instruction expects a primitvie value");
			}
			Vm_Data a = _a.data;
			Vm_Data b = _b.data;
			Vm_Stack_Object so = (Vm_Stack_Object){.type = VSOT_PRIMITIVE, .data = (Vm_Data){.prm = a.prm*b.prm}};
			da_append(Vm_Stack_Object, &VM_STACK, so);
		} break; // IS_MUL
	case IS_DIV:
		{
			Vm_Stack_Object _a = da_pop(&VM_STACK);
			Vm_Stack_Object _b = da_pop(&VM_STACK);
			if (_a.type != VSOT_PRIMITIVE || _b.type != VSOT_PRIMITIVE) LOG_ERROR(COMPONENT, "`div` instruction expects a primitvie value");
			Vm_Data a = _a.data;
			Vm_Data b = _b.data;
			Vm_Stack_Object so = (Vm_Stack_Object){.type = VSOT_PRIMITIVE, .data = (Vm_Data){.prm = a.prm/b.prm}};
			da_append(Vm_Stack_Object, &VM_STACK, so);
		} break; // IS_DIV

	// struct
	case IS_STRUCT_CREATE:
		{
			Vm_Struct strct;
			Vm_Stack_Object count_obj = da_pop(&VM_STACK);
			if (count_obj.type != VSOT_PRIMITIVE) LOG_ERROR(COMPONENT, "`struct_create` expects a primitive value for the field count");
			size_t field_count = (size_t)count_obj.data.prm;
			da_init(Vm_Stack_Object, &strct, field_count);
			for (size_t i = 0; i < field_count; i++) {
				Vm_Stack_Object field = da_pop(&VM_STACK);
				da_append(Vm_Stack_Object, &strct, field);
			}
			Vm_Stack_Object so = (Vm_Stack_Object){.type = VSOT_STRUCT, .data = (Vm_Data){.strct = strct}};
			da_append(Vm_Stack_Object, &VM_STACK, so);
		} break; // IS_STRUCT_CREATE
	case IS_STRUCT_PUSH_FIELD:
		{
			Vm_Stack_Object index_obj = da_pop(&VM_STACK);
			if (index_obj.type != VSOT_PRIMITIVE) LOG_ERROR(COMPONENT, "`struct_push_field` expects a primitive value for an index");
			size_t field_index = (size_t)index_obj.data.prm;
			Vm_Stack_Object obj = da_pop(&VM_STACK);
			if (obj.type != VSOT_STRUCT) LOG_ERROR(COMPONENT, "`struct_push_field` expects a struct");
			Vm_Struct strct = obj.data.strct;
			Vm_Stack_Object so = strct.items[field_index];
			da_append(Vm_Stack_Object, &VM_STACK, so);
		} break; // IS_STRUCT_PUSH_FIELD

	// flow
	case IS_GOTO:
		{
			Vm_Stack_Object obj = da_pop(&VM_STACK);
			if (obj.type != VSOT_PRIMITIVE) LOG_ERROR(COMPONENT, "`goto` expects a primitive value");
			Dvmcg_Primitive p = obj.data.prm;
			size_t idx = (size_t)p;
			VM_STATE.pc = idx;
		} break;
	case IS_JMPREL:
		{
			Vm_Stack_Object obj = da_pop(&VM_STACK);
			if (obj.type != VSOT_PRIMITIVE) LOG_ERROR(COMPONENT, "`jmprel` expects a primitive value for the offset");
			int64_t offset = (int64_t)obj.data.prm;
			if (VM_STATE.pc + offset < 0) LOG_FATAL(COMPONENT, "Cannot jump before 1st instruction. offset = %ld, VM_STATE.pc + offset = %ld", offset, VM_STATE.pc + offset);
			VM_STATE.pc += offset;
		} break;
	case IS_RET:
		{
			Vm_Stack_Object obj = da_pop(&VM_STACK);
			if (obj.type != VSOT_PRIMITIVE) LOG_ERROR(COMPONENT, "`ret` expects a primitive value");
			VM_STATE.pc = (size_t)obj.data.prm;
		} break;

	default:
		LOG_FATAL(COMPONENT, "Encountered an unrecognizable instruction: %zu", instr->type);
		break;
	}
}

