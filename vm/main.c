#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "vm.h"
#include "component.h"
#include "libdvmcodegen/instrs.h"
#include "libdvmcodegen/module.h"
#include "libcommon/log.h"
#include "libcommon/da.h"

int main(int argc, char **argv)
{
	const char *input_file_path = argv[1];
	FILE *input_file = fopen(input_file_path, "rb");
	if (input_file == NULL) LOG_FATAL(COMPONENT, "Cannot open input file %s: %s", input_file_path, strerror(errno));

	Dvmcg_Bytes bytes = {0};
	da_init(unsigned char, &bytes, 1);
	unsigned char byte;
	while (fread(&byte, 1, 1, input_file) > 0) da_append(unsigned char, &bytes, byte);

	Dvmcg_Module module = module_deserialize(&bytes);

	vm_init(&module);
	while (VM_STATE.running && VM_STATE.pc < module.count) {
		printf("VM_STATE.pc = %u\n", VM_STATE.pc);
		Dvmcg_Meta_Instr *instr = vm_instr_fetch();
		LOG_DEBUG(COMPONENT, "current instruction: %s", dvmcg_instr_set_to_string(instr->type));
		vm_instr_eval(instr);
		LOG_DEBUG(COMPONENT, "stack top: %f", VM_STACK.items[VM_STACK.count-1].data.prm);
	}
	vm_deinit();
	module_deinit(&module);
	fclose(input_file);

	return 0;
}
