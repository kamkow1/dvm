#ifndef VM_H_
#define VM_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

#include "libdvmcodegen/instrs.h"
#include "libdvmcodegen/module.h"

typedef struct {
	bool running;
	uint32_t pc;
} Vm_State;

typedef struct Vm_Stack_Object Vm_Stack_Object;

typedef struct {
	Vm_Stack_Object *items;
	size_t count;
	size_t cap;
} Vm_Struct;

typedef union {
	Dvmcg_Primitive prm;
	Vm_Struct strct;
} Vm_Data;

typedef enum {
	VSOT_PRIMITIVE,
	VSOT_STRUCT,
} Vm_Stack_Object_Type;

struct Vm_Stack_Object {
	Vm_Data data;
	Vm_Stack_Object_Type type;
};

typedef struct {
	Vm_Stack_Object *items;
	size_t count;
	size_t cap;
} Vm_Stack;

extern Vm_State VM_STATE;
extern Vm_Stack VM_STACK;
extern Dvmcg_Meta_Instr *PROGRAM;

// init
void vm_init(Dvmcg_Module *m);
void vm_deinit(void);
void vm_init_state(void);
void vm_deinit_state(void);

// instr
Dvmcg_Meta_Instr *vm_instr_fetch(void);
void vm_instr_eval(const Dvmcg_Meta_Instr *instr);

#endif // VM_H_
