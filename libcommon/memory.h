#ifndef LIBCOMMON_MEMORY_H_
#define LIBCOMMON_MEMORY_H_

#ifndef MALLOC
	#define MALLOC malloc
#endif

#ifndef REALLOC
	#define REALLOC realloc
#endif

#endif // LIBCOMMON_MEMORY_H_
