#ifndef STR_H_
#define STR_H_

// dynamically allocated string structure
typedef struct {
	char *items;
	size_t count;
	size_t cap;
} Str;

// construct Str from a c string
Str str_from_cstr(const char *cstr);

#endif // STR_H_
