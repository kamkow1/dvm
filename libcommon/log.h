#ifndef LIBCOMMON_LOG_H_
#define LIBCOMMON_LOG_H_

#include <stdio.h>
#include <stdlib.h>

// component is a prefix of a component that displays the message
// for example: `as - DEBUG: new label: main+0x0000`
// comes from:
// LOG_DEBUG(COMPONENT, "new label: %s+0x%04x", l.string.items, (unsigned int)l.instr_idx);
//
// NOTE: use `LOG_ERROR()` only to report recoverable errors.
// if you cannot handle an error or the error makes further execution impossible, use `LOG_FATAL()`

#define LOG_ERROR(component, fmt, ...) { printf(component" - ERROR: "fmt"\n", ##__VA_ARGS__); }
#define LOG_FATAL(component, fmt, ...) { printf(component" - FATAL: "fmt"\n", ##__VA_ARGS__); exit(EXIT_FAILURE); }
#define LOG_INFO(component, fmt, ...)  { printf(component" - INFO: "fmt"\n", ##__VA_ARGS__); }
#define LOG_DEBUG(component, fmt, ...) { printf(component" - DEBUG: "fmt"\n", ##__VA_ARGS__); }

#endif // LIBCOMMON_LOG_H_
