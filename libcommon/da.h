#ifndef LIBCOMMON_DA_H_
#define LIBCOMMON_DA_H_

#include "memory.h"

// Dynamic array requirements:
// - X *items
// - size_t count
// - size_t cap
//
// In order, for the array to be usable, it MUST contain those three fields
// with the exact same names
//
// example dynamic array struct:
//
// typedef struct {
// 		int *items;
// 		size_t count;
// 		size_t cap;
// } Dynamic_Array;

// initialize a generic dynamic array
#define da_init(type, da, initcap) \
{ \
	(da)->items = MALLOC(initcap * sizeof(type)); \
	(da)->count = 0; \
	(da)->cap = initcap; \
} \

// deinitialize a dynamic array (reset counter and free the buffer)
#define da_deinit(da) \
{ \
	(da)->count = 0; \
	free((da)->items); \
} \

// append to a dybamic array.
// resize the buffer if out of memory
#define da_append(type, da, item) \
{ \
	if ((da)->count == (da)->cap) { \
		(da)->cap *= 2; \
		(da)->items = REALLOC((da)->items, (da)->cap * sizeof(type)); \
	} \
	(da)->items[(da)->count++] = item; \
} \

// pop from a dynamic array.
// this function doesn't deallocate any memory, it just "forgets" it.
// the memory can be later overwritten or just deallocated when calling `da_deinit()`
#define da_pop(da) ((da)->items[--(da)->count])

#endif // LIBCOMMON_DA_H_
