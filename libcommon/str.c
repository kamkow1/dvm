#include <string.h>
#include <stdlib.h>

#include "str.h"
#include "libcommon/da.h"

Str str_from_cstr(const char *cstr)
{
	Str str;
	da_init(char, &str, strlen(cstr));

	char c;
	while ((c = *cstr++)) da_append(char, &str, c);
	return str;
}
