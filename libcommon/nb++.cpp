#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <filesystem>

#include "../nb++.hpp"

enum SubcommandIndex
{
	SBI_BUILD,
	SBI_RUN,
};

static const char *subcommands[] =
{
	[SBI_BUILD] = "build",
};

int main(int argc, char **argv)
{
	std::tuple<bool, nb::OptMap> opts_result = nb::parse_options(argc, argv);
	bool ok = std::get<bool>(opts_result);
	if (!ok) {
		nb::log_error("failed to parse commandline options");
		return 1;
	}
	nb::OptMap optmap = std::get<nb::OptMap>(opts_result);
	nb::Configuration configuration = nb::my_configuration; // defined in nb++-configuration.hpp

	// C++	
	if (optmap.contains("c++")) {
		std::string cplusplus = std::get<std::string>(optmap["c++"]);
		std::tuple<bool, std::string> recom_compiler = nb::find_program(cplusplus, {});
		if (!std::get<0>(recom_compiler)) {
			nb::log_error("recom_compiler: not found");
			return 1;
		}
		configuration.recom_compiler = std::get<1>(recom_compiler);
	} else nb::log_warning("No C++ compiler selected. Defaulting to " + configuration.recom_compiler);
			
	if (optmap.contains("c")) {
		std::string c = std::get<std::string>(optmap["c"]);
		std::tuple<bool, std::string> compiler = nb::find_program(c, {});
		if (!std::get<0>(compiler)) {
			nb::log_error("compiler: not found " + c);
			return 1;
		}
		configuration.compiler = std::get<1>(compiler);
	} else nb::log_warning("No C compiler selected. Defaulting to " + configuration.compiler);
			
	if (optmap.contains("ar")) {
		std::string ar = std::get<std::string>(optmap["ar"]);
		std::tuple<bool, std::string> archiver = nb::find_program(ar, {});
		if (!std::get<0>(archiver)) {
			nb::log_error("archiver: not found " + ar);
			return 1;
		}
		configuration.archiver = std::get<1>(archiver);
	} else nb::log_warning("No archiver selected. Defaulting to " + configuration.archiver);
	configuration.archiver += " rcs";

	if (optmap.contains("sc")) {
		std::string subcmd = std::get<std::string>(optmap["sc"]);
		nb::log_info("Running subcommand: " + subcmd);
		if (subcmd == subcommands[SBI_BUILD]) {
			std::tuple<bool, bool> result = nb::check_if_changed();
			if (!std::get<0>(result)) return 1; // nb will log the internal error

			if (std::get<0>(result) && std::get<1>(result)) {
				if (!nb::recompile_self(argc, argv, configuration)) {
					nb::log_error("nb++ failed to recompile itself");
					return 1;
				} else nb::rerun_self(argc, argv);
			}

			std::vector<std::string> src = nb::src(".c", {});

			// basic
			nb::Flags compile_flags;
			compile_flags.push(
				"-c",
				"-Wextra",
				"-Wall",
				"-o",
				"#object#.o"
			);

			// include paths
			compile_flags.push("-I.", "-I..", "-lc");

			std::vector<std::string> obj = nb::compile(src, configuration, compile_flags);

			nb::Flags archive_flags;

			// basic
			archive_flags.push("libcommon.a");

			nb::archive(obj, configuration, archive_flags, NB_FLAGS_FIRST);
		}
	} else {
		nb::log_error("Please select a subcommand to run using `-sc` option");
		nb::log_info("Available subcommands:");
		for (size_t i = 0; i < sizeof(subcommands)/sizeof(subcommands[0]); ++i) nb::log_info(subcommands[i]);

		return 1;
	}

	return 0;
}
