#include "instrs.h"
#include "component.h"
#include "libcommon/log.h"

const char *dvmcg_instr_set_to_string(Dvmcg_Instr_Set is)
{
	switch (is) {
	case IS_PUSH: return "IS_PUSH";
	case IS_POP: return "IS_POP";
	case IS_HANG: return "IS_HANG";
	case IS_KILL: return "IS_KILL";
	case IS_ADD: return "IS_ADD";
	case IS_SUB: return "IS_SUB";
	case IS_MUL: return "IS_MUL";
	case IS_DIV: return "IS_DIV";
	case IS_STRUCT_CREATE: return "IS_STRUCT_CREATE";
	case IS_STRUCT_PUSH_FIELD: return "IS_STRUCT_PUSH_FIELD";
	case IS_GOTO: return "IS_GOTO";
	case IS_JMPREL: return "IS_JMPREL";
	case IS_RET: return "IS_RET";
	case _size: LOG_FATAL(COMPONENT, "unreachable"); break;
	default: LOG_FATAL(COMPONENT, "unhandled instruction %s", dvmcg_instr_set_to_string(is)); break;
	}
}
