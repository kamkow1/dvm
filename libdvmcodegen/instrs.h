#ifndef LIBDVMCODEGEN_INSTRS_H_
#define LIBDVMCODEGEN_INSTRS_H_

#include <stdint.h>
#include <stddef.h>

// different instruction types
typedef enum {
	// stack
	IS_PUSH,
	IS_POP,

	// vm state
	IS_KILL,
	IS_HANG,

	// math
	IS_ADD,
	IS_SUB,
	IS_MUL,
	IS_DIV,

	// struct
	IS_STRUCT_CREATE,
	IS_STRUCT_PUSH_FIELD,

	// flow
	IS_GOTO,
	IS_JMPREL,
	IS_RET,

	// enforce enum size
	_size = UINT64_MAX,
} Dvmcg_Instr_Set;

// primitive Vm's type
typedef double Dvmcg_Primitive;

// instruction metadata
typedef struct {
	Dvmcg_Instr_Set type;
	// operands
	Dvmcg_Primitive *items;
	size_t count;
	size_t cap;
} Dvmcg_Meta_Instr;

const char *dvmcg_instr_set_to_string(Dvmcg_Instr_Set is);

#endif // LIBDVMCODGEN_INSTRS_H_
