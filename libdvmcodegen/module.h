#ifndef LIBDVMCODEGEN_MODULE_H_
#define LIBDVMCODEGEN_MODULE_H_

#include <stddef.h>

#include "instrs.h"
#include "libcommon/da.h"

// module - a dynamic array of instructions
typedef struct {
	Dvmcg_Meta_Instr *items;
	size_t count;
	size_t cap;
} Dvmcg_Module;

#define module_deinit(m) \
{ \
	for (size_t i = 0; i < (m)->count; i++) { \
		da_deinit(&(m)->items[i]); \
	} \
	da_deinit((m)); \
} \

// dynamic byte array
typedef struct {
	unsigned char *items;
	size_t count;
	size_t cap;
} Dvmcg_Bytes;

// serialize a module into a byte array
Dvmcg_Bytes module_serialize(Dvmcg_Module *m);
// deserialize a module from a byte array
// caller has to deallocate the module
Dvmcg_Module module_deserialize(Dvmcg_Bytes *bs);

#endif // LIBDVMCODEGEN_MODULE_H_
