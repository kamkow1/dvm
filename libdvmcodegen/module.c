#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>

#include "module.h"
#include "instrs.h"
#include "component.h"
#include "libcommon/da.h"
#include "libcommon/log.h"

Dvmcg_Bytes module_serialize(Dvmcg_Module *m)
{
	Dvmcg_Bytes bytes;
	da_init(unsigned char, &bytes, 1);

	// serialize the number of instructions
	const unsigned char *count_bytes = (unsigned char *)&m->count;
	const size_t count_bytes_size = sizeof(size_t)/sizeof(unsigned char);
	for (size_t i = 0; i < count_bytes_size; i++) {
		unsigned char byte = count_bytes[i];
		da_append(unsigned char, &bytes, byte);
	}

	for (size_t i = 0; i < m->count; i++) {
		Dvmcg_Meta_Instr meta_instr = m->items[i];
	
		// serialize the type of an instruction
		const unsigned char *type_bytes = (unsigned char *)&meta_instr.type;
		const size_t type_bytes_size = sizeof(uint64_t)/sizeof(unsigned char);
		for (size_t j = 0; j < type_bytes_size; j++) {
			unsigned char byte = type_bytes[j];
			da_append(unsigned char, &bytes, byte);
		}

		// serialize it's operands
		for (size_t k = 0; k < meta_instr.count; k++) {
			Dvmcg_Primitive operand = meta_instr.items[k];
			const size_t operand_bytes_size = sizeof(Dvmcg_Primitive)/sizeof(unsigned char);
			const unsigned char *operand_bytes = (unsigned char *)&operand;
			for (size_t l = 0; l < operand_bytes_size; l++) {
				unsigned char byte = operand_bytes[l];
				da_append(unsigned char, &bytes, byte);
			}
		}
	}
	return bytes;
}

Dvmcg_Module module_deserialize(Dvmcg_Bytes *bs)
{
#define parse_n_ops(n) \
{ \
	for (size_t x = 0; x < n; x++) { \
		Dvmcg_Bytes tmp_op; \
		da_init(unsigned char, &tmp_op, 1); \
		size_t s = cursor+sizeof(Dvmcg_Primitive); \
		for (;cursor < s; cursor++) da_append(unsigned char, &tmp_op, bs->items[cursor]); \
		Dvmcg_Primitive p = *(Dvmcg_Primitive*)tmp_op.items; \
		da_append(Dvmcg_Primitive, &instr, p); \
		LOG_DEBUG(COMPONENT, "instruction operand = %f", p); \
		da_deinit(&tmp_op); \
	} \
} \

	LOG_INFO(COMPONENT, "deserializing a module...");
	size_t cursor = 0;

	Dvmcg_Module module;
	da_init(Dvmcg_Meta_Instr, &module, 1);

	// deserialize instruction count
	size_t instr_count = 0;
	{
		Dvmcg_Bytes tmp;
		da_init(unsigned char, &tmp, 1);
		size_t s = cursor+sizeof(size_t);
		for (;cursor < s; cursor++) da_append(unsigned char, &tmp, bs->items[cursor]);
		instr_count = *(size_t*)tmp.items;
		LOG_INFO(COMPONENT, "instr_count = %zu", instr_count);
		da_deinit(&tmp);
	}

	// deserialize all instructions
	for (size_t i = 0; i < instr_count; i++) {
		Dvmcg_Meta_Instr instr;
		da_init(Dvmcg_Primitive, &instr, 1);

		// deserialize instruction type
		Dvmcg_Bytes tmp_instr;
		da_init(unsigned char, &tmp_instr, 1);
		size_t s = cursor+sizeof(uint64_t);
		for (;cursor < s; cursor++) da_append(unsigned char, &tmp_instr, bs->items[cursor]);
		instr.type = (Dvmcg_Instr_Set)*(uint64_t*)tmp_instr.items;
		LOG_INFO(COMPONENT, "instr.type = %s", dvmcg_instr_set_to_string(instr.type));
		da_deinit(&tmp_instr);

		// deserialize the operands
		switch (instr.type) {
		case IS_PUSH: parse_n_ops(1); break; // IS_PUSH
		case IS_STRUCT_CREATE: break; // IS_STRUCT_CREATE
		case IS_STRUCT_PUSH_FIELD: break; // IS_STRUCT_PUSH_FIELD
		case IS_GOTO: break; // IS_GOTO
		case IS_JMPREL: break; // IS_JMPREL
		case IS_RET: break; // IS_RET
		case IS_POP: break; // IS_POP
		case IS_ADD: break; // IS_ADD
		case IS_SUB: break; // IS_SUB
		case IS_DIV: break; // IS_DIV
		case IS_MUL: break; // IS_MUL
		case IS_KILL: break; // IS_KILL
		case IS_HANG: break; // IS_HANG
		case _size: LOG_FATAL(COMPONENT, "unreachable"); break;
		}
		da_append(Dvmcg_Meta_Instr, &module, instr);
	}

	return module;
}
