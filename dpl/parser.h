#ifndef PARSER_H_
#define PARSER_H_

#include <stddef.h>

#include "token.h"
#include "libcommon/str.h"	

typedef struct Stmt Stmt;

typedef struct {
	Stmt *items;
	size_t count;
	size_t cap;
} Stmts;

typedef struct {
	Str name;
	Stmts stmts;
} Stmt_Proc_Def;

typedef struct {
	Str name;
} Stmt_Proc_Decl;

typedef enum {
	ST_PROC_DECL,
	ST_PROC_DEF,
} Stmt_Type;

typedef union {
	Stmt_Proc_Decl proc_decl;
	Stmt_Proc_Def proc_def;
} Stmt_Data;

struct Stmt {
	Stmt_Data data;
	Stmt_Type type;
};


void stmts_deinit(Stmts *s);
void debug_print_stmts(Stmts *stmts);
Stmts parse_statements(Token_Stream *ts);

#endif // PARSER_H_
