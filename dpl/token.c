#include <string.h>
#include <ctype.h>
#include <stddef.h>

#include "token.h"
#include "component.h"
#include "libcommon/log.h"

void token_classify(Token *t)
{
	if (strcmp(t->string.items, "proc") == 0) {
		t->type = TT_PROC;
	} else if (strcmp(t->string.items, "define-proc") == 0) {
		t->type = TT_DEFINE_PROC;
	} else if (strcmp(t->string.items, "{") == 0) {
		t->type = TT_LBRACE;
	} else if (strcmp(t->string.items, "}") == 0) {
		t->type = TT_RBRACE;
	} else if (strcmp(t->string.items, ";") == 0) {
		t->type = TT_SEMICOLON;
	} else {
		if (isalpha(t->string.items[0])) { // begin identifier
			for (size_t i = 1; i < t->string.count; i++) {
				if (!isalnum(t->string.items[i])) goto unknown_token;
			}
			t->type = TT_IDENT;
		} else {
unknown_token:
			LOG_ERROR(COMPONENT, "unknown token: %s", t->string.items);
		}
	}
}

Token *ts_next_token(Token_Stream *ts)
{
	if (ts->current_index == ts->count) return NULL;
	return &ts->items[ts->current_index++];
}
