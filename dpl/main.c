#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "component.h"
#include "token.h"
#include "libcommon/log.h"
#include "libcommon/da.h"
#include "libcommon/str.h"
#include "parser.h"

int main(int argc, char **argv)
{
	if (argc < 2) LOG_FATAL(COMPONENT, "No input file");

	const char *input_file_path = argv[1];
	FILE *input_file = fopen(input_file_path, "r");
	if (input_file == NULL) LOG_FATAL(COMPONENT, "Cannot open input file %s: %s", input_file_path, strerror(errno));

	Token_Stream ts;
	da_init(Token, &ts, 1);

	char linebuf[0xff];
	memset(linebuf, 0, sizeof(linebuf));
	const char *delims = " \t\r\n\v\f";
	while (fgets(linebuf, sizeof(linebuf), input_file)) {
		char *rest = NULL;
		char *token = NULL;
		for ((token = strtok_r(linebuf, delims, &rest));
			token != NULL;
			token = strtok_r(NULL, delims, &rest)) {
			da_append(Token, &ts, (Token){.string = str_from_cstr(token)});
		}
	}

	for (size_t i = 0; i < ts.count; i++) {
		printf("%s\n", ts.items[i].string.items);
	}

	Stmts stmts = parse_statements(&ts);

	debug_print_stmts(&stmts);

	stmts_deinit(&stmts);
	da_deinit(&ts);
	fclose(input_file);

	return 0;
}
