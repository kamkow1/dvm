#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "component.h"
#include "parser.h"
#include "token.h"
#include "libcommon/da.h"
#include "libcommon/log.h"
#include "libcommon/str.h"

Stmts parse_statements(Token_Stream *ts)
{
#define check_token_null(_token) \
{ \
	if ((_token) == NULL) LOG_FATAL(COMPONENT, "unexpected end of token stream"); \
} \

#define check_token_type(_token, current, expected_next, expected_next_enum) \
{ \
	if ((_token)->type != expected_next_enum) LOG_ERROR(COMPONENT, "expected "expected_next" after "current); \
} \

	Stmts stmts;
	da_init(Stmt, &stmts, 1);

	Token *token = NULL;
	while ((token = ts_next_token(ts)) != NULL) {
		token_classify(token);
		switch (token->type) {
		case TT_PROC:
			{
				Stmt stmt = {0};
				stmt.type = ST_PROC_DECL;

				token = ts_next_token(ts);
				check_token_null(token);
				token_classify(token);
				check_token_type(token, "keyword `proc`", "an identifier", TT_IDENT);
				stmt.data = (Stmt_Data){.proc_decl = (Stmt_Proc_Decl){.name = str_from_cstr(token->string.items)}};
				da_append(Stmt, &stmts, stmt);
			} break; // TT_PROC
		case TT_DEFINE_PROC:
			{
				Stmt stmt = {0};
				stmt.type = ST_PROC_DEF;

				token = ts_next_token(ts);
				check_token_null(token);
				token_classify(token);
				check_token_type(token, "keyword `define-proc`", "an identifier", TT_IDENT);
				stmt.data = (Stmt_Data){.proc_def = (Stmt_Proc_Def){.name = str_from_cstr(token->string.items)}};
				da_init(Stmt, &stmt.data.proc_def.stmts, 1);

				token = ts_next_token(ts);
				check_token_null(token);
				token_classify(token);
				check_token_type(token, "an identifier", "a `{` symbol", TT_LBRACE);
				
				Token_Stream body = {0};
				da_init(Token, &body, 1);

				while ((token = ts_next_token(ts)) != NULL) {
					token_classify(token);
					if (token->type == TT_RBRACE) break;
					else da_append(Token, &body, *token);
				}

				Stmts body_stmts = parse_statements(&body);
				for (size_t i = 0; i < body_stmts.count; i++) da_append(Stmt, &stmt.data.proc_def.stmts, body_stmts.items[i]);
				check_token_null(token);
				if (token->type != TT_RBRACE) LOG_ERROR(COMPONENT, "unclosed procedure definition block");
				da_append(Stmt, &stmts, stmt);
				ts_deinit(&body);
				// leak body_stmts - it will be cleaned up later
			} break; // TT_DEFINE_PROC
		default: LOG_ERROR(COMPONENT, "unhandled token type: %d", token->type); break;
		}
	}

	return stmts;
}

void stmts_deinit(Stmts *s)
{
	for (size_t i = 0; i < s->count; i++) {
		switch (s->items[i].type) {
		case ST_PROC_DECL:
			{
				da_deinit(&s->items[i].data.proc_decl.name);
			} break;
		case ST_PROC_DEF:
			{
				da_deinit(&s->items[i].data.proc_def.name);
				stmts_deinit(&s->items[i].data.proc_def.stmts);
			} break;
		}
	}
	da_deinit((s));
}

void debug_print_stmts(Stmts *stmts)
{
	for (size_t i = 0; i < stmts->count; i++) {
		Stmt stmt = stmts->items[i];
		switch (stmt.type) {
		case ST_PROC_DECL:
			{
				LOG_DEBUG(COMPONENT, "Stmt: ST_PROC");
				LOG_DEBUG(COMPONENT, "    name: %s", stmt.data.proc_def.name.items);
			} break;
		case ST_PROC_DEF:
			{
				LOG_DEBUG(COMPONENT, "Stmt: ST_DEFINE_PROC");
				LOG_DEBUG(COMPONENT, "    name: %s", stmt.data.proc_def.name.items);
				debug_print_stmts(&stmt.data.proc_def.stmts);
			} break;
		}
	}
}
